Description
===========
<!-- Add here descriptions what changes, why it is required, usecases, ... !-->


Conformity
==================

- [ ] [Changelog entry](https://invent.kde.org/education/labplot/-/blob/master/ChangeLog)
- [ ] Downport
- [ ] Unittests
- Fixes: <!-- Add here the bugs you fixed. You can add multiple by separating them by a comma !-->
<!-- - [ ] [Dokumentation] (TODO) !-->

If external dependencies are removed
==================
- [ ] Reporting to ...
